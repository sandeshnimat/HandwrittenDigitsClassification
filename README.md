### Handwritten Digits Classification
<!-- will add information as development progresses -->
In this project, we implemented a Multilayer Perceptron Neural Network and evaluate its
performance in classifying handwritten digits. 
After completing this project, we were able to understand:
* How Neural Network works and use Feed Forward, Back Propagation to implement Neural Network?
* How to setup a Machine Learning experiment on real data?
* How regularization plays a role in the bias-variance tradeoff?